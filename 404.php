<?php
/**
* @package Hope for Tomorrow
*/
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="row">
				<div class="twelve columns">

					<section class="error-404 not-found">
						<header class="page-header">
							<h2 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'hope_for_tomorrow' ); ?></h2>
						</header>

						<div class="page-content row">
							<div class="twelve columns">
								<p><?php _e( 'It looks like nothing was found at this location. Maybe try one of the links below?', 'hope_for_tomorrow' ); ?></p>

								<div class="widget widget_categories">
									<h2 class="widget-title"><?php _e( 'Pages', 'hope_for_tomorrow' ); ?></h2>
									<?php wp_list_pages( 'title_li=' ); ?>
								</div>

							</div>
						</div>
					</section>
				</div>
			</div>

		</main>
	</div>

<?php get_footer(); ?>