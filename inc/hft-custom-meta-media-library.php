<?php

/**
 * Add Photographer Name and URL fields to media uploader
 *
 * @param $form_fields array, fields to include in attachment form
 * @param $post object, attachment record in database
 * @return $form_fields, modified form fields
 */
  
function be_attachment_field_credit( $form_fields, $post ) {
	
	$foo = get_post_meta( $post->ID, 'include_attachment_in_banner', true );
	
    $form_fields['include-attachment-in-banner'] = array(
        'label' => 'Include in banner',
        'input' => 'html',
        'html' => '<label for="attachments-'.$post->ID.'-include-attachment-in-banner"> '.
        '<input type="checkbox" id="attachments-'.$post->ID.'-include-attachment-in-banner" name="attachments['.$post->ID.'][include-attachment-in-banner]" value="1"'.($foo ? ' checked="checked"' : '').' /> Yes</label>  ',
        'value' => get_post_meta( $post->ID, 'include_attachment_in_banner', true ),
        'helps' => 'If left unchecked this image will not appear in the header of the page.',
    );

    return $form_fields;

}
 
add_filter( 'attachment_fields_to_edit', 'be_attachment_field_credit', 10, 2 );
 
/**
 * Save values of Photographer Name and URL in media uploader
 *
 * @param $post array, the post data for database
 * @param $attachment array, attachment fields from $_POST form
 * @return $post array, modified post data
 */
 
function be_attachment_field_credit_save( $post, $attachment ) {
	
    if( isset( $attachment['include-attachment-in-banner'] ) ) {
	    update_post_meta( $post['ID'], 'include_attachment_in_banner', $attachment['include-attachment-in-banner'] );
    }

    return $post;
}
 
add_filter( 'attachment_fields_to_save', 'be_attachment_field_credit_save', 10, 2 );
?>