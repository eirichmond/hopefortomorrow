<?php
function hft_users_roles() {
	add_role(
		'bp_member',
		'Buddy Press Member',
		array(
			'read' => true,
		)
	);
}
add_action( 'after_setup_theme', 'hft_users_roles' );

function redirect_non_bp_member() {
    if (!current_user_can( 'administrator' ) || !current_user_can( 'bp_member' )) {
        wp_redirect( home_url( '/wp-admin/' ) );
        exit();
    }
}

function redirect_unauthorised_users()
{
    if( is_page('activity') && !is_user_logged_in() ) {
	    redirect_non_bp_member();
    }
    if( is_page('members') && !is_user_logged_in() ) {
	    redirect_non_bp_member();
    }
    if( is_page('groups') && !is_user_logged_in() ) {
	    redirect_non_bp_member();
    }
}
add_action( 'template_redirect', 'redirect_unauthorised_users' );

/**
 * Redirect user after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 * @return string
 */

function hft_login_redirect( $redirect_to, $request, $user ) {
	//is there a user to check?
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		//check for admins
		if ( in_array( 'administrator', $user->roles ) ) {
			// redirect them to the default place
			return $redirect_to;
		} elseif (in_array( 'bp_member', $user->roles)) {
			return home_url('/activity/');
		} else {
			return home_url();
		}
	} else {
		return $redirect_to;
	}
}

add_filter( 'login_redirect', 'hft_login_redirect', 10, 3 );


?>