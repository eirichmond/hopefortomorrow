<?php

remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_breadcrumb', 20 );

add_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper');
function woocommerce_output_content_wrapper() {
	echo '<div class="eight columns">';
}

add_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end');
function woocommerce_output_content_wrapper_end() {
		echo '</div>';
}


/*
 * Override via functions.php
 **/
if (!function_exists('woocommerce_template_loop_add_to_cart')) {
	function woocommerce_template_loop_add_to_cart() {
		global $product;
		if ( ! $product->is_in_stock() || ! $product->is_purchasable() ) {
			echo '<a href="'.get_permalink().'" rel="nofollow" class="outstock_button">Out of Stock</a>';

		} else {
			woocommerce_get_template('loop/add-to-cart.php');
		}
		
	}
}