<?php 
	
function get_geo_location_by_postcode($postcode) {
	
	$postcode = htmlentities($postcode);
	
	$request = 'http://api.postcodes.io/postcodes/'.$postcode;
	
	$curl = curl_init();
	
	curl_setopt_array($curl, array(
		CURLOPT_URL => $request,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"Cache-Control: no-cache",
			"Postman-Token: a39ab230-a3cb-4005-895d-8fdd62dda298"
		),
	));
	
	$response = curl_exec($curl);
	$err = curl_error($curl);
	
	curl_close($curl);
	
	if ($err) {
		echo "cURL Error #:" . $err;
	} else {
		$response = json_decode($response);
	}
	
	$lon = (float)$response->result->longitude;
	$lat = $response->result->latitude;
	
	$array = array(
		$lon,
		(float)$lat
	);
	
	return $array;
	
}

function get_mcu_locations() {
	
	// Get any existing copy of our transient data
	if ( false === ( $array = get_transient( 'mccu_location_units' ) ) ) {
		
		// It wasn't there, so regenerate the data and save the transient
		$array = array(
			'type' => 'FeatureCollection',
			'features' => array(
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('GL205GJ')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Gloucestershire',
						'address' => 'Tewkesbury Community Hospital, Barton Rd',
						'city' => 'Tewkesbury',
						'county' => 'Gloucestershire',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('GL14 3HX')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Gloucestershire',
						'address' => 'Dilke Memorial Hospital',
						'city' => 'Cinderford',
						'county' => 'Gloucestershire',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('GL7 1UY')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Gloucestershire',
						'address' => 'Cirencester Hospital, Tetbury Rd',
						'city' => 'Cirencester',
						'county' => 'Gloucestershire',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('GL5 2HY')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Gloucestershire',
						'address' => 'Stroud General Hospital, Trinity Rd',
						'city' => 'Stroud',
						'county' => 'Gloucestershire',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('TA20 2LW')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Taunton &amp; Somerset',
						'address' => 'Tesco, Tapstone Rd',
						'city' => 'Chard',
						'county' => 'Taunton &amp; Somerset',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('TA24 6DF')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Taunton &amp; Somerset',
						'address' => 'Minehead Community Hospital, Mart Rd, Luttrell Way',
						'city' => 'Minehead',
						'county' => 'Taunton &amp; Somerset',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('BA6 8JD')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Taunton &amp; Somerset',
						'address' => 'West Mendip Community Hospital, Old Wells Rd',
						'city' => 'Glastonbury',
						'county' => 'Taunton &amp; Somerset',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('TA6 4GU')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Taunton &amp; Somerset',
						'address' => 'Bridgwater Community Hospital Bower Lane',
						'city' => 'Bridgwater',
						'county' => 'Taunton &amp; Somerset',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('SP6 1JD')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Salisbury',
						'address' => 'Drill Hall Bartons Rd',
						'city' => 'Fordingbridge',
						'county' => 'Salisbury',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('SP8 4FA')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Salisbury',
						'address' => 'Peacemarsh Surgery, Marlott Road, Gillingham',
						'city' => 'Dorset',
						'county' => 'Salisbury',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('BA13 3FQ')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Salisbury',
						'address' => 'White Horse Health Centre, Mane Way',
						'city' => 'Westbury',
						'county' => 'Salisbury',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('CT6 6EB')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'East Kent Hospitals',
						'address' => 'Queen Victoria Memorial Hospital, King Edward Ave',
						'city' => 'Herne Bay',
						'county' => 'East Kent',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('TN24 0LZ')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'East Kent Hospitals',
						'address' => 'William Harvey Hospital, Kennington Rd, Willesborough',
						'city' => 'Ashford',
						'county' => 'East Kent',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('CT14 7EQ')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'East Kent Hospitals',
						'address' => 'Deal Rugby Club, The Drill Field, Canada Road, Walmer',
						'city' => 'Deal',
						'county' => 'East Kent',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('LN2 5QY')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Lincolnshire',
						'address' => 'Lincoln County Hospital, Greetwell Rd',
						'city' => 'Lincoln',
						'county' => 'Lincolnshire',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('LN11 0EU')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Lincolnshire',
						'address' => 'County Hospital Louth, High Holme Rd',
						'city' => 'Louth',
						'county' => 'Lincolnshire',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('NG31 8DG')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Lincolnshire',
						'address' => 'Grantham and District Hospital, Manthorpe Rd',
						'city' => 'Grantham',
						'county' => 'Lincolnshire',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('PE25 2BS')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Lincolnshire',
						'address' => 'Skegness and District Hospital, Dorothy Ave',
						'city' => 'Skegness',
						'county' => 'Lincolnshire',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('TR13 8PJ')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Cornwall',
						'address' => 'Tesco, Clodgey Ln',
						'city' => 'Helston',
						'county' => 'Cornwall',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('PL25 3EF')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Cornwall',
						'address' => '1 Wheal Northey',
						'city' => 'St Austell',
						'county' => 'Cornwall',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('IP14 5BE')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'West Suffolk',
						'address' => 'Tesco Superstore, Cedars Link Road',
						'city' => 'Stowmarket',
						'county' => 'West Suffolk',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('CB8 7AH')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'West Suffolk',
						'address' => 'Tesco Superstore, Fordham Road',
						'city' => 'Newmarket',
						'county' => 'West Suffolk',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('CO10 2DZ')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'West Suffolk',
						'address' => 'Sudbury Community Health Centre, Church Field Road',
						'city' => 'Sudbury',
						'county' => 'West Suffolk',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('IP24 1DDA')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'West Suffolk',
						'address' => 'Thetford Healthy Living Centre, Croxton Road',
						'city' => 'Thetford',
						'county' => 'West Suffolk',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('CO9 1JA')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Colchester',
						'address' => 'Butler Road Car Park',
						'city' => 'Halstead',
						'county' => 'Colchester',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('CO5 0SU')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Colchester',
						'address' => 'Tesco, 86 Church Rd',
						'city' => 'Tiptree',
						'county' => 'Colchester',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('CO3 0LX')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Colchester',
						'address' => 'Co-op, Stanway Retail Park, Peartree Rd ',
						'city' => 'Colchester',
						'county' => 'Colchester',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('CO15 4EF')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Colchester',
						'address' => 'Tesco, Brook Retail Park, London Rd',
						'city' => 'Clacton-on-Sea',
						'county' => 'Colchester',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('SE25 6XB')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Royal Marsden',
						'address' => 'Sainsbury’s, 120-122 Whitehorse Lane London ',
						'city' => 'London',
						'county' => 'Royal Marsden',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('CR0 5RA')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Royal Marsden',
						'address' => 'Lloyd Park, 84 Coombe Rd',
						'city' => 'Croydon',
						'county' => 'Royal Marsden',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('KT17 4RJ')
					),
					'properties' => array(
						'type' => 'MCCU',
						'name' => 'Royal Marsden',
						'address' => 'Deport Road Car Park',
						'city' => 'Epsom',
						'county' => 'Royal Marsden',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('SO41 8ZZ')
					),
					'properties' => array(
						'type' => 'NSV',
						'name' => 'Southampton',
						'address' => 'Oakhaven Hospice Trust, Lower Pennington Lane',
						'city' => 'Lymington',
						'county' => 'Southampton',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('SO30 3JB')
					),
					'properties' => array(
						'type' => 'NSV',
						'name' => 'Southampton',
						'address' => 'Southampton',
						'city' => 'Southampton',
						'county' => 'Southampton',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('BD24 9BP')
					),
					'properties' => array(
						'type' => 'NSV',
						'name' => 'Booths',
						'address' => 'Booths',
						'city' => 'Kirkgate',
						'county' => 'Settle',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('BB8 9NW')
					),
					'properties' => array(
						'type' => 'NSV',
						'name' => 'Boundary Mill Stores',
						'address' => 'Vivary Way',
						'city' => 'Colne',
						'county' => 'Colne',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('BD16 1BE')
					),
					'properties' => array(
						'type' => 'NSV',
						'name' => 'Woodbank Nurseries',
						'address' => 'Harden Rd',
						'city' => 'Harden',
						'county' => 'Bingley',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('LS29 8EE')
					),
					'properties' => array(
						'type' => 'NSV',
						'name' => 'Booths at Ilkley',
						'address' => 'Leeds Rd',
						'city' => 'Ilkley',
						'county' => 'Ilkley',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('LS29 7HR')
					),
					'properties' => array(
						'type' => 'NSV',
						'name' => 'Generous Pioneer',
						'address' => 'Burley in Wharfedale, 2 Ilkley Rd',
						'city' => 'Burley in Wharfedale',
						'county' => 'Ilkley',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('BD23 1TS')
					),
					'properties' => array(
						'type' => 'NSV',
						'name' => 'Morrisons at Skipton',
						'address' => '2 Broughton Rd',
						'city' => 'Skipton',
						'county' => 'Skipton',
					)
				),
				array(
					'type' => 'Feature',
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => get_geo_location_by_postcode('BD21 3RU')
					),
					'properties' => array(
						'type' => 'NSV',
						'name' => 'Sainsburys Centre',
						'address' => '1 Cavendish St',
						'city' => 'Keighley',
						'county' => 'Keighley',
					)
				),
			)	
		);
	
		set_transient( 'mccu_location_units', $array, 12 * HOUR_IN_SECONDS );
	}
	
	return json_encode($array);
}
