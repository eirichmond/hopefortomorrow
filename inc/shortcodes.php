<?php
	
/**
 * Hide email from Spam Bots using a shortcode.
 *
 * @param array  $atts    Shortcode attributes. Not used.
 * @param string $content The shortcode content. Should be an email address.
 *
 * @return string The obfuscated email address. 
 */
function wpcodex_hide_email_shortcode( $atts , $content = null ) {
	if ( ! is_email( $content ) ) {
		return;
	}

	return '<a href="mailto:' . antispambot( $content ) . '">' . antispambot( $content ) . '</a>';
}
add_shortcode( 'email', 'wpcodex_hide_email_shortcode' );

/**
 * shortcode for wrapped in sociallink.
 *
 * @param array  $atts    Shortcode attributes. Not used.
 * @param string $content The shortcode content. Should be an email address.
 *
 * @return string the social link. 
 */
function social_link_function( $atts , $content = null ) {

	return '<ul class="social-links">' . apply_filters( 'the_content', $content )  . '</ul>';

}
add_shortcode( 'sociallinks', 'social_link_function' );


/**
 * shortcode for social link must be wrapped in sociallinks.
 *
 * @param array  $atts    Shortcode attributes. Not used.
 * @param string $content The shortcode content. Should be an email address.
 *
 * @return string the social link. 
 */
function social_links_function( $atts , $content = null ) {

	// Attributes
	$a = shortcode_atts(
		array(
			'url' => 'http://www.twitter.com',
		),
		$atts
	);

	return '<li class="social-link"><a href="'. esc_attr($a['url']) .'">' . $content . '</a></li>';

}
add_shortcode( 'sociallink', 'social_links_function' );


/**
 * big button wrapper.
 *
 * @param array  $atts    Shortcode attributes. Not used.
 * @param string $content The shortcode content. Should be an email address.
 *
 * @return string The obfuscated email address. 
 */
function htf_bigbutton_function( $atts , $content = null ) {
	$xml = new SimpleXMLElement($content);
	
	$json = json_encode($xml);
	$array = json_decode($json,TRUE);
	
	$href = $array['@attributes']['href'];
	$text = $array[0];
	
		
	return '<div class="large oval primary btn"><a href="'.esc_attr( $href ).'">'.esc_html( $text ).'</a></div>';

}
add_shortcode( 'bigbutton', 'htf_bigbutton_function' );

/**
 * big button wrapper.
 *
 * @param array  $atts    Shortcode attributes. Not used.
 * @param string $content The shortcode content. Should be an email address.
 *
 * @return string The obfuscated email address. 
 */
function htf_bigbutton_download_function( $atts , $content = null ) {
	$xml = new SimpleXMLElement($content);
	
	$json = json_encode($xml);
	$array = json_decode($json,TRUE);
	
	$href = $array['@attributes']['href'];
	$text = $array[0];
	
		
	return '<div class="large oval primary btn icon-left entypo icon-download"><a href="'.esc_attr( $href ).'">'.esc_html( $text ).'</a></div>';

}
add_shortcode( 'bigbuttondownload', 'htf_bigbutton_download_function' );
