<?php
/*
	Template Name: Where our Units go
*/

get_header(); ?>

	<main id="main" class="site-main" role="main">
		
		<?php get_template_part('partials/unit-locations-content'); ?>

		<?php //get_template_part('partials/news-and-events'); ?>

		<?php //get_template_part('partials/newsletter-container'); ?>

	</main>

<?php get_footer(); ?>