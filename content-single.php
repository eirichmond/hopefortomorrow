<?php
/**
 * @package Hope for Tomorrow
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row">
		<div class="four columns">
			<?php if (has_post_thumbnail()) {
				the_post_thumbnail('full');
			} ?>
		</div>
		<div class="eight columns">
			<header class="entry-header">
				<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
		
				<div class="entry-meta">
					<?php hope_for_tomorrow_posted_on(); ?>
				</div>
			</header>
		
			<div class="entry-content">
				<?php
					the_content( sprintf(
						__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'hope_for_tomorrow' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					) );
				?>
			</div>
		</div>
	</div>

</article>