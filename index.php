<?php
/**
* The main template file.
* @package Hope for Tomorrow
*/

get_header(); ?>

	<div id="primary" class="content-area content-container">
		<main id="main" class="site-main" role="main">

			<div class="row">
				<div class="twelve columns">
					<?php if ( have_posts() ) : ?>

						<?php while ( have_posts() ) : the_post(); ?>

							<?php
								get_template_part( 'content', get_post_format() );
							?>

						<?php endwhile; ?>

					<?php else : ?>

						<?php get_template_part( 'content', 'none' ); ?>

					<?php endif; ?>
				</div>
			</div>

		</main>
	</div>

<?php get_footer(); ?>