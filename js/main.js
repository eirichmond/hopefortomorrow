// Gumby is ready to go
Gumby.ready(function() {

	Gumby.log('Gumby is ready to go...', Gumby.dump());

	// placeholder polyfil
	if(Gumby.isOldie || Gumby.$dom.find('html').hasClass('ie9')) {
		jQuery('input, textarea').placeholder();
	}

	// skip link and toggle on one element
	// when the skip link completes, trigger the switch
	jQuery('#skip-switch').on('gumby.onComplete', function() {
		jQuery(this).trigger('gumby.trigger');
	});



	jQuery('.icon-plus-squared').on('touchStart',function(){
		jQuery(this).next().css("position", "relative");
	});


	jQuery('.fa-search').on( "click", function(){
			
		if (jQuery(this).hasClass('opened')){
			jQuery(this).removeClass('opened');
			jQuery('.search-open').css('width','0px').css('margin-left', '-1px');
		}else{
			jQuery(this).addClass('opened');

			//Stop the overspill
			var winSize = jQuery(window).width();
			if ( winSize > 769 && winSize < 960 ){
				jQuery('.search-open').css('width','170px');
			}else{
				jQuery('.search-open').css('width','250px');
			}
		}
		return false;
	});
	
/*
	jQuery('select[name="donation-type"').on('change', function (){
		if (jQuery(this).val() ==  'monthly') {
			jQuery('label.radio').removeClass('checked').find( '.icon-dot' ).detach();
			jQuery('#donateother').slideUp();
		} else {
			jQuery('#donateother').slideDown();
		}
	});
*/
	
	// bind to radio button check event
	jQuery('#donateother').on('gumby.onCheck', function(e) {
		if (jQuery(this).find('.checked').find('input').val() === 'other') {
			jQuery('#showdonateother').slideDown();
		} else {
			jQuery('#showdonateother').slideUp();
		}
	// dynamically check the radio button
	}).trigger('gumby.check');
	
	jQuery('.slideshow').cycle({
	    speed: 600,
	    manualSpeed: 100,
	    slides: '>span'
	});	
	
	
jQuery('#donationdeets').validation({
	// pass an array of required field objects
/*
	required: [
		{
			name: 'donation-amount-other',
			// pass a function to the validate property for complex custom validations
			// the function will receive the jQuery element itself, return true or false depending on validation
			validate: function($el) {
				var donationType = jQuery('#donateother').find('.checked').find('input').val();
				if (donationType === 'other') {
					var donationValue = jQuery('#showdonateother').find('input').val();
					if (donationValue > 0) {
						return true;
					}
				} else {
					return true;
				}
			}
		}
	],
*/
	  // pass an array of required field objects
	  required: [
	    {
	      name: 'full-name',
	    },
	    {
	      name: 'billing-address-line-1',
	    },
	    {
	      name: 'billing-address-town',
	    },
	    {
	      name: 'billing-address-county',
	    },
	    {
	      name: 'billing-address-postcode',
	    },
	    {
	      name: 'billing-address-town',
	    },
	    {
	      name: 'billing-address-town',
	    },
	    {
	      name: 'email-address',
	      // pass a function to the validate property for complex custom validations
	      // the function will receive the jQuery element itself, return true or false depending on validation
	      validate: function($el) {
	        return $el.val().match('@') !== null;
	      }
	    }
	  ],

	// callback for failed validaiton on form submit
  fail: function() {
    Gumby.error('Form validation failed');
  },
	// callback for successful validation on form submit
	// if omited, form will submit normally
/*
	submit: function(data) {
		alert("Submitting");
		jQuery.ajax({
			url: 'do/something/with/data',
			data: data,
			success: function() {alert("Submitted");}
		});
	} 
*/
});


//Else, oldie document loaded
}
).oldie(function() {
	Gumby.warn("This is an oldie browser...");

// Touch devices loaded
}).touch(function() {
	Gumby.log("This is a touch enabled device...");
});