<?php
/**
* @package Hope for Tomorrow
*/
get_header(); 
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="row">
				<div class="twelve columns">
					<div class="content-container">
						<?php if ( have_posts() ) : ?>
	
	<!--
							<header class="page-header">
								<?php
									the_archive_title( '<h2 class="page-title">', '</h2>' );
									the_archive_description( '<div class="taxonomy-description">', '</div>' );
								?>
							</header>
	-->
	
							<?php while ( have_posts() ) : the_post(); ?>

								<?php
									get_template_part( 'content', 'newsletter-archive' );
								?>
	
							<?php endwhile; ?>
	
						<?php else : ?>
	
							<?php get_template_part( 'content', 'none' ); ?>
	
						<?php endif; ?>
					</div>
				</div>
			</div>
		</main>
	</div>
<?php get_footer(); ?>