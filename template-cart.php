<?php
/**
* Template Name: Shop Page
* @package Hope for Tomorrow
*/

get_header(); ?>

	<main id="main" class="site-main" role="main">
		
		<div class="row">
			<div class="twelve columns">
				
				 <!-- Start the Loop. -->
				 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				
				 	<?php get_template_part('content-page'); ?>
				
				 <?php endwhile; else : ?>
				 	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
				 <?php endif; ?>
			</div>
		</div>

	</main>

<?php get_footer(); ?>