<?php
/**
* The template for displaying search results pages.
* @package Hope for Tomorrow
*/

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="row">
				<div class="twelve columns">
					<?php if ( have_posts() ) : ?>
						<header class="page-header">
							<h2 class="page-title"><?php printf( __( 'Search Results for: %s', 'hope_for_tomorrow' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
						</header>

						<?php while ( have_posts() ) : the_post(); ?>

							<?php
								get_template_part( 'content', 'search' );
							?>

						<?php endwhile; ?>

					<?php else : ?>
						
						<?php get_template_part( 'content', 'none' ); ?>
								
					<?php endif; ?>
				</div>
			</div>
		</main>
	</section>

<?php get_footer(); ?>