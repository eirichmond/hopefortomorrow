<div class="row">
	
	<div class="content-container">
		<div class="four columns">
			<?php get_sidebar('buddypress'); ?>
		</div>
		<div class="eight columns">
			
			 <!-- Start the Loop. -->
			 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header>
				
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
				
					<footer class="entry-footer">
						<?php edit_post_link( __( 'Edit', 'hope_for_tomorrow' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</article>			

			 <?php endwhile; else : ?>
			 	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			 <?php endif; ?>
		</div>
	</div>
</div>
