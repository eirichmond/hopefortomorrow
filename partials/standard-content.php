
<?php get_template_part( 'partials/featured-image' ); ?>

<?php get_template_part( 'partials/tabbed-nav' ); ?>


<?php
if ($post->post_parent == 0) {
	$menu_id = get_the_id();
} else {
	$menu_id = $post->post_parent;
}

$args = array(
	'post_parent' => $menu_id,
	'post_type'   => 'page', 
	'numberposts' => -1,
	'post_status' => 'publish',
	'order' => 'ASC',
	'orderby' => 'menu_order',
);
$children = get_children( $args );
?>


<div class="row">
	
	<div class="content-container">
				<?php if ($children) { ?>
					<div class="four columns">
						<ul id="child-links">
							<?php foreach ($children as $child) { ?>
								<li><a href="<?php echo get_permalink($child->ID); ?>"><?php echo get_the_title( $child->ID ); ?></a></li>
							<?php } ?>
						</ul>
					</div>
				<?php } else { ?>


					<?php  wp_nav_menu(
						array(
						    'theme_location' => 'homepage',
						    'container' => 'div',
						    'container_id' => 'child-links',
						    'container_class' => 'parentmenu',
						    'menu_class' => 'four columns',
						)
					);
					?>

	
				<?php } ?>
	
		<div class="eight columns">
			
			 <!-- Start the Loop. -->
			 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			
			 	<?php get_template_part('content-page'); ?>
			
			 <?php endwhile; else : ?>
			 	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			 <?php endif; ?>
		</div>
	</div>
</div>
