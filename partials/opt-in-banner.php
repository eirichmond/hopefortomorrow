<div class="opt-in-banner">
	<div class="row">
		
		<a href="#" class="switch" gumby-trigger="#modal1">
			
			<h2>Stay in touch</h2> 
			<h3>Please provide permission for us <span class="permissions"><i class="icon-check"></i></span></h3>
			<p class="content">We would like to stay in contact with you, to share our latest news and events details. Please share your preferences with us...</p>
			
			
		
		<div class="modal" id="modal1">
			<div class="content">
				<a class="close switch" gumby-trigger="|#modal1"><i class="icon-cancel" /></i></a>
				<div class="row">
					<div class="ten columns centered text-center">
						<?php echo do_shortcode('[ninja_form id=6]'); ?>
					</div>
				</div>
			</div>
		</div>
		
		</a>
	
   	</div>
</div>

