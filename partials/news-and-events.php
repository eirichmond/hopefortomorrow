<?php
$categories = get_categories( array(
    'orderby' => 'name',
    'order'   => 'ASC'
) );
 
foreach( $categories as $category ) {
    $category_link = sprintf( 
        '<a href="%1$s" alt="%2$s">%3$s</a>',
        esc_url( get_category_link( $category->term_id ) ),
        esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ),
        esc_html( $category->name )
    );
} ?>

<div class="row">
	
	<div class="content-container">
		
		<div class="nine columns">
			<ul id="news-and-events">
				
				<?php foreach( $categories as $category ) {
				    $category_link = sprintf( 
				        '<a href="%1$s" alt="%2$s">%3$s</a>',
				        esc_url( get_category_link( $category->term_id ) ),
				        esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ),
				        esc_html( $category->name )
				    );
				    $term_image_id = get_term_meta( $category->term_id, 'featured_image', true);
				    $image = wp_get_attachment_image( $term_image_id, 'full' );
				    $category_image_link = sprintf( 
				        '<a href="%1$s" alt="%2$s">%3$s</a>',
				        esc_url( get_category_link( $category->term_id ) ),
				        esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ),
				        $image
				    );
				    
					 ?>
					<li class="item">
						<div class="item-inner">
							<?php  echo '<h1 class="cat-title">' . sprintf( esc_html__( '%s', 'textdomain' ), $category_link ) . '</h1> '; ?>
							<?php echo $category_image_link; ?>
						</div>
					</li>
				<?php } ?>
			</ul>
	
		</div>
		<div class="three columns">
			<div class="nesidebar">
				<?php get_sidebar(); ?>
			</div>
		</div>
	
	</div>
	
</div>