<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.31.0/mapbox-gl.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.31.0/mapbox-gl.css' rel='stylesheet' />


<div class="row">
	<div class="twelve columns">
		
		<h2 class="patients-header">Where our MCCUs go</h2>
		<p>Use the interactive map below to see where our Mobile Cancer Care Units visit.</p>
		
		<div class="mapcontainer">
			<div class='sidebar pad2'>
				<div class='heading'>
					<h1>Mobile Cancer Care Units</h1>
				</div>
				<div id='listings' class='listings'></div>
			</div>
			<div id='map' class='map pad2'></div>
		</div>
	</div>
</div>

<?php $data = get_mcu_locations(); ?>

<script>
  // This will let you use the .remove() function later on
  if (!('remove' in Element.prototype)) {
    Element.prototype.remove = function() {
      if (this.parentNode) {
          this.parentNode.removeChild(this);
      }
    };
  }

  mapboxgl.accessToken = 'pk.eyJ1IjoiZWxsaW90dHJpY2htb25kIiwiYSI6IjA2ODQ2ZDcxYjRkNGI5NjY1ZDI1ZGU4MzYxODY4N2I5In0.ZSGtUtslGsJJGaf7LmppKQ';

  // This adds the map
  var map = new mapboxgl.Map({
    // container id specified in the HTML
    container: 'map', 
    // style URL
    style: 'mapbox://styles/elliottrichmond/cizscmlaa008k2rkw3gij91vw', 
    // initial position in [long, lat] format
    center: [-2.089748, 51.795748], 
    // initial zoom
    zoom: 5.7
  });

  var stores = JSON.parse( '<?php echo $data; ?>' );
  
  // This adds the data to the map
  map.on('load', function (e) {
    // This is where your '.addLayer()' used to be, instead add only the source without styling a layer
    map.addSource("places", {
      "type": "geojson",
      "data": stores
    });
    // Initialize the list
    buildLocationList(stores);
 
  });
  
  // This is where your interactions with the symbol layer used to be
  // Now you have interactions with DOM markers instead
  stores.features.forEach(function(marker, i) {
    // Create an img element for the marker
    var el = document.createElement('div');
    el.id = "marker-" + i;
    el.className = 'marker';
    el.style.left='-28px';
    el.style.top='-46px';
    // Add markers to the map at all points
    new mapboxgl.Marker(el)
        .setLngLat(marker.geometry.coordinates)
        .addTo(map); 

    el.addEventListener('click', function(e){
        // 1. Fly to the point
        flyToStore(marker);

        // 2. Close all other popups and display popup for clicked store
        createPopUp(marker);
        
        // 3. Highlight listing in sidebar (and remove highlight for all other listings)
        var activeItem = document.getElementsByClassName('active');

        e.stopPropagation();
        if (activeItem[0]) {
           activeItem[0].classList.remove('active');
        }

        var listing = document.getElementById('listing-' + i);
        listing.classList.add('active');

    });
  });

  
  function flyToStore(currentFeature) {
    map.flyTo({
        center: currentFeature.geometry.coordinates,
        zoom: 15
      }); 
  }

  function createPopUp(currentFeature) {
    var popUps = document.getElementsByClassName('mapboxgl-popup');
    if (popUps[0]) popUps[0].remove();


    var popup = new mapboxgl.Popup({closeOnClick: false})
          .setLngLat(currentFeature.geometry.coordinates)
          .setHTML(
          	'<h3>' + currentFeature.properties.type + '</h3>' + 
            '<h4>' + currentFeature.properties.name + '</h4>' + 
            '<p>'+currentFeature.properties.address+'</p>' + 
            '<p>'+currentFeature.properties.county+'</p>'
          )
          .addTo(map);
  }
 

  function buildLocationList(data) {
    for (i = 0; i < data.features.length; i++) {
      var currentFeature = data.features[i];
      var prop = currentFeature.properties;
      
      var listings = document.getElementById('listings');
      var listing = listings.appendChild(document.createElement('div'));
      listing.className = 'item';
      listing.id = "listing-" + i;
      
      var link = listing.appendChild(document.createElement('a'));
      link.href = '#';
      link.className = 'title';
      link.dataPosition = i;
      link.innerHTML = prop.name;     
      
      var details = listing.appendChild(document.createElement('div'));
      details.innerHTML = prop.city;
      if (prop.county) {
        details.innerHTML += ' &middot; ' + prop.county;
      }



      link.addEventListener('click', function(e){
	      e.preventDefault();
        // Update the currentFeature to the store associated with the clicked link
        var clickedListing = data.features[this.dataPosition];
        
        // 1. Fly to the point
        flyToStore(clickedListing);

        // 2. Close all other popups and display popup for clicked store
        createPopUp(clickedListing);
        
        // 3. Highlight listing in sidebar (and remove highlight for all other listings)
        var activeItem = document.getElementsByClassName('active');

        if (activeItem[0]) {
           activeItem[0].classList.remove('active');
        }
        this.parentNode.classList.add('active');
        
        


      });
    }
  }


</script>