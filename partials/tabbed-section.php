<?php
	

	//Only show the nav when there's child pages.
	$all_menu_items = has_children( get_the_id() );
	
	if ( !empty( $all_menu_items ) ) {
	?>

		<div class="tabbed-pages-container">
			<div class="row">
		    	<div class="twelve columns">
				    <section class="tabs vertical">
						<ul class="tab-nav">
							<?php
								$i=1;
								foreach( $all_menu_items as $menu_item )
								{
									?>
										<li <?php if ( $i==1 ){ echo 'class="active"'; } ?>><a href="<?php echo esc_attr( $menu_item['link'] ); ?>"><?php echo esc_html( $menu_item['title'] ); ?></a></li>
									<?php
									$i++;
								}
							?>
						</ul>

							<?php
								$a = 0;

								foreach( $all_menu_items as $tab_content )
								{
									$content_id = $tab_content['post-content'];

									$tab_content = events_excerpt_shortening();
								?>
									<div class="tab-content<?php if( $a === 0 ){ echo esc_attr( ' active' ); } ?>">
										<?php echo apply_filters( 'the_content', $content_id ); ?>
							        </div>
								<?php
								$a++;
								}

							?>
					</section>
				</div>
			</div>
		</div>
	<?php
	}else{
	?>
	<div class="no-tabs-pages-container">
		<div class="row">
	    	<div class="nine columns centered">
				 <!-- Start the Loop. -->
				 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				
				 	<?php get_template_part('content-page'); ?>
				
				 <?php endwhile; else : ?>
				 	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
				 <?php endif; ?>
	    	</div>
	    </div>
	</div>
	<?php
	}
	?>
