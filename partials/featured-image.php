<?php

// Get any existing copy of our transient data
if ( false === ( $slides = get_transient( 'include_attachment_in_banner' ) ) ) {
  // It wasn't there, so regenerate the data and save the transient
  
  	//$all_medias = get_attached_media( 'image', $post->ID);
  	
  	$args = array(
	  	'posts_per_page' => -1,
	  	'post_type' => 'attachment',
	  	'meta_key' => 'include_attachment_in_banner'
  	);
  	
  	$slides = get_posts($args);

/*
	$slides = array();
	foreach ($all_medias as $all_media) {
		$include_in_banner = get_post_meta( $all_media->ID, 'include_attachment_in_banner', true );
		if (!empty($include_in_banner)) {
			$slides[] = $all_media->ID;
		}
	}
*/

	set_transient( 'include_attachment_in_banner', $slides, 60*60*24 );
}
// Use `$all_medias` like you would have normally...

?>

<div class="cycle-slideshow"
	data-cycle-slides="span"
	data-cycle-speed=2000
	data-cycle-loader=true
>
	
	<?php if ($slides) { ?>
	
		<?php foreach ($slides as $featured_image_id) { ?>

			<span class="sliders">
				<?php echo wp_get_attachment_image( $featured_image_id->ID, 'large-feature'); ?>
				<?php if(is_front_page()) { ?>
					<h1 class="featured-image-title"><?php the_title(); ?></h1>
				<?php } ?>
			</span>

		<?php } ?>
		
	<?php } else { ?>

		<?php $featured_image_id = get_post_thumbnail_id( get_the_id() );
		if ($featured_image_id) {
			$featured_image = wp_get_attachment_image_src( $featured_image_id, 'full');
		}
		if ($featured_image) { ?>

		<div class="sliders" style="background-image:url(<?php echo $featured_image[0]; ?>)">
				<?php if(is_front_page()) { ?>
					<h1 class="featured-image-title"><?php the_title(); ?></h1>
				<?php } ?>
		</div>
		
		<?php } else { ?>
			<div id="no-featured-image"></div>
		<?php } ?>

	
	<?php } ?>
	
</div>