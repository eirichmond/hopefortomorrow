<div class="our-units-container">
    <div class="row">
    	<div class="eight columns">
    		<h3 class="our-units-title"><a href="#" class="switch" gumby-trigger="#mcuits">Where our MCCUs go, click here</a></h3>
    		<strong class="our-units-desc">Find out where our Mobile Cancer Care Units visit.</strong>
    	</div>
    	<div class="four columns map-link">
	    	<a href="#" class="switch map-image-bg-link" gumby-trigger="#mcuits"></a>
    	</div>
    </div>
</div>

<div class="modal" id="mcuits">
	<div class="content">
		<a class="close switch" gumby-trigger="|#mcuits"><i class="icon-cancel" /></i></a>
		<div class="row">
			<div class="twelve columns centered text-center">
			
				<img src="<?php echo get_template_directory_uri(); ?>/img/mu-locations.jpg" alt="Mobile Chemotherapy Unit locations" width="2339" height="1654" />
			
			</div>
		</div>
	</div>
</div>
