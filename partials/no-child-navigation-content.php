
<?php get_template_part( 'partials/featured-image' ); ?>

<?php get_template_part( 'partials/tabbed-nav' ); ?>



<div class="row">
	
	<div class="content-container">
		<div class="twelve columns">
			
			 <!-- Start the Loop. -->
			 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			
			 	<?php get_template_part('content-page'); ?>
			
			 <?php endwhile; else : ?>
			 	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			 <?php endif; ?>
		</div>
	</div>
</div>
