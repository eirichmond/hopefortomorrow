<div class="header-top-container">
	<div class="row">
		<div class="four columns logo">
			<?php if (has_custom_logo()) { ?>
				
				<?php the_custom_logo(); ?>
				<h1 class="text-logo-sf"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>

				
				
			<?php } else { ?>
			
				<h1 class="text-logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>	
								
			<?php } ?>
		</div>
		<div class="four columns">
			
			<ul class="tilethis">
				
				<li class="donate-button">
					<a href="/donate-hope-for-tomorrow/donate-details/">Donate <span><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span></a>
				</li>
				<li class="awards">
					<span class="queens-award"></span>
				</li>

			</ul>

		</div>
		
		<div class="three columns push_one">
			<?php get_search_form(); ?>
		</div>
		
	</div>
</div>

<div class="navbar header-bottom-container" id="nav1">
	
	
	<div class="row">
		
		<div class="menunav">
			
			<a class="toggle" gumby-trigger="#primary-menu" href="#"><i class="icon-menu"></i></a>
			
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => false, 'menu_class' => 'primary-menu', 'menu_id' => 'primary-menu', 'walker' => new dropdownmenu_class() ) ); ?>
			
		</div>
		
		<div class="social-icons-container">

			<?php wp_nav_menu( array( 'theme_location' => 'social-icons-menu', 'container' => false, 'menu_class' => 'social-icons-list', 'menu_id' => 'social-icons-menu' ) ); ?>

		</div>
		
	</div>
	
	
</div>


