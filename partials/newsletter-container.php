<div class="newsletter-container">
	<div class="row">
		<h2 class="newsletter-header">Sign up for our newsletter...</h2>
	</div>
	<div class="row">
		<div class="twelve columns"><?php echo do_shortcode('[ninja_form id=4]'); ?></div>
   	</div>
</div>