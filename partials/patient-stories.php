<?php

	$args = array(
		'post_type' => 'patient-stories',
		'posts_per_page' => 3,
		'post_status' => 'publish'
	);

	$the_query = new WP_Query( $args );

/*
	if ( $the_query->have_posts() )
	{
		?>

		<div class="patient-stories-container">
			<div class="row">
				<h2 class="patients-header">Patient Stories</h2>
			</div>
			<div class="row">

			<?php while ( $the_query->have_posts() ) 
			{
			$the_query->the_post();
			?>

				<a href="<?php the_permalink(); ?>" class="four columns patient-story-linked">
		   			<?php echo the_post_thumbnail( 'front-patient-stories' ); ?>
		   			<h3 class="patient-name"><?php the_title(); ?></h3>
		   			<?php the_excerpt(); ?>
				</a>

			<?php
			}
			wp_reset_postdata();
			?>

			</div>

			   	<div class="row">
					<div class="three columns push_nine patient-stories-button-container">
						<button class="orange-button patient-stories-button">
							<a href="<?php echo home_url() . '/patient-stories/'; ?>">More Patient Stories <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
						</button>
					</div>
				</div>
		</div>
		<?php
		}
*/
?>

		<?php if ( $the_query->have_posts() ) : ?>
		
			<!-- pagination here -->

			<div class="patient-stories-container">
				
				<div class="row">
					<div class="twelve columns">
						<h2 class="patients-header">Patient Stories</h2>
					</div>
				</div>
	
				<div class="row">

					<!-- the loop -->
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<div class="four columns">
							<a href="<?php the_permalink(); ?>" class="patient-story-linked">
								<span class="inner-patient-wrapper">
									<?php if (has_post_thumbnail()) {?>
										<?php the_post_thumbnail('front-patient-stories'); ?>
									<?php } ?>
									<h3 class="patient-name"><?php the_title(); ?></h3>
									<?php echo wp_trim_words( get_the_content(), 40, '...' ); ?>
								</span>
							</a>							
						</div>
					<?php endwhile; ?>
					<!-- end of the loop -->
				
					<!-- pagination here -->
				
					<?php wp_reset_postdata(); ?>

				</div>
	
			</div>
		
		<?php else : ?>
			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif; ?>
		
	   	<div class="row">
			<div class="four columns push_eight">
				
				<div class="patient-stories-button-container">
					<div class="medium primary btn icon-right entypo icon-right-circled"><a href="<?php echo home_url() . '/patient-stories/'; ?>">More Patient Stories</a></div>
				</div>

			</div>
		</div>
