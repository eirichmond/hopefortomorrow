<div class="tabbed-menu-container">
	<div class="row">
        <?php wp_nav_menu( array( 'theme_location' => 'tabbed', 'container' => false, 'menu_class' => 'tabbed-menu twelve columns', 'menu_id' => 'tabbed' ) ); ?>
        
        <!-- this should be removed when instructed -->
        <div class="twelve columns">
	        <ul id="tabbed" class="tabbed-menu">
		        <li>
<!-- 		        	<a href="#">&nbsp;</a> -->
		        </li>
	        </ul>
        </div>
        <!-- this should be removed when instructed -->

    </div>
</div>
