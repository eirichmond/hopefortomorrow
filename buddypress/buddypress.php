<?php
/*
* @package Hope for Tomorrow
*/

get_header(); ?>

	<main id="main" class="site-main" role="main">
		
		
		<?php
			//var_dump($wp_query);
			if (current_user_can( 'administrator' ) || current_user_can( 'bp_member' )) {
				
				if (is_page('activity')) {
					get_template_part('buddypress/buddypress-activity');
				} else {
					get_template_part('buddypress/buddypress-content');
				}
			
			}
		?>

		<?php //get_template_part('partials/patient-stories'); ?>

		<?php //get_template_part('partials/our-units'); ?>

		<?php //get_template_part('partials/news-and-events'); ?>

		<?php //get_template_part('partials/newsletter-container'); ?>

	</main>

<?php get_footer(); ?>