<?php
/*
*
Template Name: NHS Portal Page
* @package Hope for Tomorrow
*/

if (!is_user_logged_in()) {
	wp_die( 'Sorry! You need to be logged in to view page.' );
}

get_header(); ?>

	<main id="main" class="site-main" role="main">
		
		<?php get_template_part('partials/nhsportal-content'); ?>

		<?php //get_template_part('partials/patient-stories'); ?>

		<?php //get_template_part('partials/our-units'); ?>

		<?php //get_template_part('partials/news-and-events'); ?>

		<?php //get_template_part('partials/newsletter-container'); ?>

	</main>

<?php get_footer(); ?>