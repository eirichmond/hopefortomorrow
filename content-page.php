<?php
/**
* @package Hope for Tomorrow
*/
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<?php if (!is_front_page()) { ?>
		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header>
	<?php } ?>

	<div class="entry-content">
		<?php the_content(); ?>
	</div>

	<footer class="entry-footer">
		<?php edit_post_link( __( 'Edit', 'hope_for_tomorrow' ), '<span class="edit-link">', '</span>' ); ?>
	</footer>
</article>