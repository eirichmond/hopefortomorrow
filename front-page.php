<?php
/**
* @package Hope for Tomorrow
*/

get_header(); ?>

	<main id="main" class="site-main" role="main">
		
		<?php get_template_part('partials/no-child-navigation-content'); ?>
		<?php //get_template_part('partials/standard-content'); ?>

		<?php get_template_part('partials/patient-stories'); ?>

		<?php get_template_part('partials/unit-locations-content'); ?>
		
		<?php //get_template_part('partials/our-units'); ?>

		<?php //get_template_part('partials/news-and-events'); ?>

		<?php //get_template_part('partials/newsletter-container'); ?>

		<?php get_template_part('partials/opt-in-banner'); ?>

		<?php get_template_part('partials/impact-counter'); ?>

	</main>

<?php get_footer(); ?>
