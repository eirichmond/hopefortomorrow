<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Hope for Tomorrow
 */

if ( ! is_active_sidebar( 'shop' ) ) {
	return;
}
?>
<div class="four columns">
	<div id="secondary" class="widget-area" role="complementary">
		<?php dynamic_sidebar( 'shop' ); ?>
	</div><!-- #secondary -->
</div>