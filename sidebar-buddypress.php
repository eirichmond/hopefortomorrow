<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Hope for Tomorrow
 */

if ( ! is_active_sidebar( 'sidebar-buddypress' ) ) {
	return;
}
?>

<div id="secondary" class="widget-area buddypress" role="complementary">
	<?php dynamic_sidebar( 'sidebar-buddypress' ); ?>
</div><!-- #secondary -->
