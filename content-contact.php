<?php
/**
* @package UoG Sustainability
*/
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<div class="row">
			<div class="twelve columns">		
				<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
				<h4 class="sub-header"><?php the_excerpt(); ?></h4>
			</div>
		</div>
	</header>

	<div class="row">
		<div class="twelve columns">
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
		</div>
	</div>

</article>