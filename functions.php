<?php
/**
* Hope for Tomorrow functions and definitions
* @package Hope for Tomorrow
*/

if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'hope_for_tomorrow_setup' ) ) :

function hope_for_tomorrow_setup() {
	
	add_theme_support( 'woocommerce' );

	load_theme_textdomain( 'hope_for_tomorrow', get_template_directory() . '/languages' );

	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'title-tag' );

	add_theme_support( 'post-thumbnails' );
	
	add_theme_support( 'custom-logo', array(
		'height'      => 213,
		'width'       => 400,
		'flex-width' => true,
		'header-text' => array( 'site-title', 'site-description' ),
	) );

	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'hope_for_tomorrow' ),
		'homepage' => __( 'Homepage Menu', 'hope_for_tomorrow' ),
		'tabbed' => __( 'Tabbed Menu', 'hope_for_tomorrow' ),
		'social-icons-menu' => __( 'Social Icons Menu', 'hope_for_tomorrow' ),
	) );

	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	add_theme_support( 'custom-background', apply_filters( 'hope_for_tomorrow_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	add_image_size('front-patient-stories', 254, 200, array( 'left', 'top' ));
	add_image_size('news-and-events', 220, 149 );
	add_image_size('large-feature', 1200, 600, true );
	
}
endif; // hope_for_tomorrow_setup
add_action( 'after_setup_theme', 'hope_for_tomorrow_setup' );


function hope_for_tomorrow_widgets_init() {

	register_sidebar( array(
		'name'          => __( 'Sidebar', 'hope_for_tomorrow' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Shop', 'hope_for_tomorrow' ),
		'id'            => 'shop',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'BuddyPress Sidebar', 'hope_for_tomorrow' ),
		'id'            => 'sidebar-buddypress',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Area 1', 'hope_for_tomorrow' ),
		'id'            => 'footer-area-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Area 2', 'hope_for_tomorrow' ),
		'id'            => 'footer-area-2',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Area 3', 'hope_for_tomorrow' ),
		'id'            => 'footer-area-3',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
/*
	register_sidebar( array(
		'name'          => __( 'Footer Area 4', 'hope_for_tomorrow' ),
		'id'            => 'footer-area-4',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
*/

	register_sidebar( array(
		'name'          => __( 'Bottom Footer Nav', 'hope_for_tomorrow' ),
		'id'            => 'bottom-footer-nav',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );



}
add_action( 'widgets_init', 'hope_for_tomorrow_widgets_init' );


function hope_for_tomorrow_scripts() {
	wp_enqueue_style( 'hope_for_tomorrow-style', get_stylesheet_uri() );
	wp_enqueue_style( 'hope_for_tomorrow-gumby-style', get_template_directory_uri() .'/css/gumby.css' );

	wp_enqueue_style( 'hope_for_tomorrow-font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', array(), '4.5.0' );

	wp_enqueue_script( 'hope_for_tomorrow-modernizr', get_template_directory_uri() . '/js/libs/modernizr-2.6.2.min.js', array(), '20120206', true );

	wp_enqueue_script( 'hope_for_tomorrow-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'hope_for_tomorrow-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	wp_enqueue_script( 'hope_for_tomorrow-gumby-js', get_template_directory_uri() . '/js/libs/gumby.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'hope_for_tomorrow-gumby-retina', get_template_directory_uri() . '/js/libs/ui/gumby.retina.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'hope_for_tomorrow-gumby-fixed', get_template_directory_uri() . '/js/libs/ui/gumby.fixed.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'hope_for_tomorrow-gumby-skiplink', get_template_directory_uri() . '/js/libs/ui/gumby.skiplink.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'hope_for_tomorrow-gumby-toggleswitch', get_template_directory_uri() . '/js/libs/ui/gumby.toggleswitch.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'hope_for_tomorrow-gumby-checkbox', get_template_directory_uri() . '/js/libs/ui/gumby.checkbox.js', array('jquery'), '20130115', true );
	//wp_enqueue_script( 'hope_for_tomorrow-gumby-radiobtn', get_template_directory_uri() . '/js/libs/ui/gumby.radiobtn.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'hope_for_tomorrow-gumby-tabs', get_template_directory_uri() . '/js/libs/ui/gumby.tabs.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'hope_for_tomorrow-gumby-navbar', get_template_directory_uri() . '/js/libs/ui/gumby.navbar.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'hope_for_tomorrow-gumby-validation', get_template_directory_uri() . '/js/libs/ui/jquery.validation.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'hope_for_tomorrow-gumby-init', get_template_directory_uri() . '/js/libs/gumby.init.js', array('jquery'), '20130115', true );


	$translation_array = array( 'templateUrl' => get_template_directory_uri() );
	wp_localize_script( 'hope_for_tomorrow-gumby-js', 'object_name', $translation_array );

	wp_enqueue_script( 'hope_for_tomorrow-cycle2', get_template_directory_uri() . '/js/libs/cycle.js', array('jquery'), '20130115', false );

	wp_enqueue_script( 'hope_for_tomorrow-gumby-plugins', get_template_directory_uri() . '/js/plugins.js', array(), '20130115', true );
	wp_enqueue_script( 'hope_for_tomorrow-gumby-main', get_template_directory_uri() . '/js/main.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'hope_for_tomorrow_scripts' );


class dropdownmenu_class extends Walker_Nav_Menu
{
    function start_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<div class=\"dropdown\"><ul>\n";
    }
    function end_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul></div>\n";
    }
}


function has_children($post_ID)
{
    
    $all_items = get_pages( array( 'child_of' => $post_ID, 'orderby' => 'menu_order', 'order' => 'ASC', 'sort_column' => 'menu_order' ) );

    $all_menu_items = array();
    $i=0;
    foreach( $all_items as $item )
    {

    	$all_menu_items[$i]['link'] = get_page_link( $item->ID );
    	$all_menu_items[$i]['title'] = $item->post_title;
		$all_menu_items[$i]['post-content'] = $item->post_content;
   		$i++;
    }

	return $all_menu_items;

}

//Bring back the shortened excerpt for news & events
/*
function events_excerpt_shortening( $excerpt )
{

	$newExcerpt = substr( $excerpt, 0, 55 );

	if ( strlen( $excerpt ) > 55 )
	{
	    return $newExcerpt . "&hellip;";
	}
	else
	{
		return $excerpt;	
	}
}

add_filter( 'the_excerpt', 'events_excerpt_shortening', 10, 1 );
*/

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 50;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

/**
 * Filter the "read more" excerpt string link to the post.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function wpdocs_excerpt_more( $more ) {
    return sprintf( '<a class="read-more" href="%1$s">%2$s</a>',
        get_permalink( get_the_ID() ),
        __( ' ...read more', 'textdomain' )
    );
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

function modify_archive_title( $title ) {
    $title = str_replace("Archives: ","",$title);
    return $title;
}
add_filter( 'get_the_archive_title', 'modify_archive_title', 10, 1 );


function create_post_types()
{

	$labels = array(
		'name'               => _x( 'Patient Stories', 'post type general name', 'hope_for_tomorrow' ),
		'singular_name'      => _x( 'Patient Story', 'post type singular name', 'hope_for_tomorrow' ),
		'menu_name'          => _x( 'Patient Stories', 'admin menu', 'hope_for_tomorrow' ),
		'name_admin_bar'     => _x( 'Patient Story', 'add new on admin bar', 'hope_for_tomorrow' ),
		'add_new'            => _x( 'Add New Story', 'Event item', 'hope_for_tomorrow' ),
		'add_new_item'       => __( 'Add New Story', 'hope_for_tomorrow' ),
		'new_item'           => __( 'New Story', 'hope_for_tomorrow' ),
		'edit_item'          => __( 'Edit Story', 'hope_for_tomorrow' ),
		'view_item'          => __( 'View Story', 'hope_for_tomorrow' ),
		'all_items'          => __( 'All Stories', 'hope_for_tomorrow' ),
		'search_items'       => __( 'Search Stories', 'hope_for_tomorrow' ),
		'parent_item_colon'  => __( 'Parent Story:', 'hope_for_tomorrow' ),
		'not_found'          => __( 'No stories found.', 'hope_for_tomorrow' ),
		'not_found_in_trash' => __( 'No stories found in Trash.', 'hope_for_tomorrow' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'hope_for_tomorrow' ),
		'public'             => true,
		'menu_icon' 		 => 'dashicons-format-status',
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'patient-stories' ),
		'capability_type'    => 'post','page',
		'has_archive'        => true,
		'hierarchical'       => true,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
	);

	register_post_type( 'patient-stories', $args );

/*
	$labels = array(
		'name'               => _x( 'News and Events', 'post type general name', 'hope_for_tomorrow' ),
		'singular_name'      => _x( 'News and Events', 'post type singular name', 'hope_for_tomorrow' ),
		'menu_name'          => _x( 'News and Events', 'admin menu', 'hope_for_tomorrow' ),
		'name_admin_bar'     => _x( 'News and Events', 'add new on admin bar', 'hope_for_tomorrow' ),
		'add_new'            => _x( 'Add New Event', 'Event item', 'hope_for_tomorrow' ),
		'add_new_item'       => __( 'Add New Event', 'hope_for_tomorrow' ),
		'new_item'           => __( 'New Event', 'hope_for_tomorrow' ),
		'edit_item'          => __( 'Edit Event', 'hope_for_tomorrow' ),
		'view_item'          => __( 'View Event', 'hope_for_tomorrow' ),
		'all_items'          => __( 'All News and Events', 'hope_for_tomorrow' ),
		'search_items'       => __( 'Search News and Events', 'hope_for_tomorrow' ),
		'parent_item_colon'  => __( 'Parent Event:', 'hope_for_tomorrow' ),
		'not_found'          => __( 'No news and events found.', 'hope_for_tomorrow' ),
		'not_found_in_trash' => __( 'No news and events found in Trash.', 'hope_for_tomorrow' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'hope_for_tomorrow' ),
		'public'             => true,
		'menu_icon' 		 => 'dashicons-calendar-alt',
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'news-and-events' ),
		'capability_type'    => 'post','page',
		'has_archive'        => true,
		'hierarchical'       => true,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
	);

	register_post_type( 'news-and-events', $args );
*/

}
add_action( 'init', 'create_post_types' );

add_filter('acf/settings/show_admin', '__return_false');

/**
 * WordPress menu functions.
 */
require get_template_directory() . '/inc/menu-functions.php';

/**
 * WordPress users and roles.
 */
require get_template_directory() . '/inc/users-roles.php';

/**
 * WordPress settings API Class.
 */
require get_template_directory() . '/inc/buddypress-functions.php';

/**
 * WordPress settings API Class.
 */
require get_template_directory() . '/inc/helper-functions.php';

/**
 * WordPress settings API Class.
 */
require get_template_directory() . '/classes/class.settings-api.php';

/**
 * Custom settings using API Class.
 */
require get_template_directory() . '/inc/site-settings.php';

/**
 * Woocommerce functions.
 */
require get_template_directory() . '/inc/woocommerce-functions.php';



/** TEMPORARY TABLE FOR DONATIONS RECORDS
 * Custom settings using API Class.
 */
require get_template_directory() . '/inc/donation-records/donations-records.php';

// //Include Human Made Custom Fields, as library not a plugin.
// require_once( 'libs/custom-meta-boxes/custom-meta-boxes.php' );

// //Include Human Made Custom Fields, as library not a plugin.
// require_once( 'inc/core-functions/config.php' );

/* * * * * * * * * * * * * * * * * * 
	
	Consider for plugin territory	
	
* * * * * * * * * * * * * * * * * */

require get_template_directory() . '/inc/hft-custom-meta-media-library.php';

/* * * * * * * * * * * * * * * * */

function myfunction () {
	$activity = get_post( 142 );
	setup_postdata($activity);
	return the_content();
	echo 'bp_before_directory_activity_content';
}

add_action( 'bp_before_directory_activity_content', 'myfunction' );



/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

require get_template_directory() . '/inc/shortcodes.php';