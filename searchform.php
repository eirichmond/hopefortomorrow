<form action="/" method="get" class="searchform">
	<div class="searcher">
		<div class="sinput"><input type="text" name="s" id="search" placeholder="search" value="<?php the_search_query(); ?>" /></div>
		<div class="sicon">
			<div class="medium default btn icon-right entypo icon-search"><input type="submit" value="Search" /></div>
		</div>
	</div>
</form>