<?php
/**
* The template for displaying the footer.
* Contains the closing of the #content div and all content after
* @package Hope for Tomorrow
*/
?>
	</div>

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="above-footer-container">
				<div class="row">
					<div class="three columns">
						<?php dynamic_sidebar('footer-area-1'); ?>
					</div>
					<div class="three columns">
						<?php dynamic_sidebar('footer-area-2'); ?>
					</div>
					<div class="six columns">
						<?php dynamic_sidebar('footer-area-3'); ?>
					</div>
<!--
					<div class="three columns">
						<?php dynamic_sidebar('footer-area-4'); ?>
					</div>
-->
				</div>
			</div>
			<div class="below-footer-container">
				<div class="row">
					<div class="twelve columns line-seperator"></div>
				</div>

				<div class="row">
					<div class="nine columns push_three">
						<div class="site-info">
							<?php dynamic_sidebar('bottom-footer-nav'); ?>
							<ul class="bottom-footer-nav">
								<li><a href="http://www.squareonemd.co.uk" rel="designer" target="_blank">Square One Web and Design</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
		</footer>
		
		<div class="logos">
			<div class="row">
				<div class="two columns">
					<img src="<?php echo bloginfo('template_url');?>/images/FR_RegLogo_HR.png" alt="FR_RegLogo_HR" />
				</div>
			</div>
		</div>
		
	</div>

	<?php wp_footer(); ?>

	</body>
	</html>