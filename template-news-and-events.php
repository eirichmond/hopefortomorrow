<?php
/*
	Template Name: News and Events
*/

get_header(); ?>

	<main id="main" class="site-main" role="main">
				
		<?php get_template_part('partials/no-child-navigation-content-no-feature'); ?>

		<?php //get_template_part('partials/patient-stories'); ?>

		<?php //get_template_part('partials/our-units'); ?>

		<?php get_template_part('partials/news-and-events'); ?>

		<?php //get_template_part('partials/newsletter-container'); ?>

	</main>

<?php get_footer(); ?>