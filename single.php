<?php
/**
* The template for displaying all single posts.
* @package Hope for Tomorrow
*/

get_header(); ?>

	<div id="primary" class="content-area content-container">
		<main id="main" class="site-main" role="main">

			<div class="row">
				<div class="twelve columns">
					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'single' ); ?>

					<?php endwhile; ?>
				</div>
			</div>
		</main>
	</div>

<?php get_footer(); ?>